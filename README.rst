========================
yin2009_eval
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/yin2009_eval/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/yin2009_eval/

.. #}
.. {# pkglts, glabreport, after doc
main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/yin2009_eval/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/yin2009_eval/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/yin2009_eval/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/yin2009_eval/commits/main


prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/b326/yin2009_eval/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/b326/yin2009_eval/commits/prod

.. |prod_coverage| image:: https://gitlab.com/b326/yin2009_eval/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/b326/yin2009_eval/commits/prod
.. #}

Evaluation of yin2009 paper, attempt to reproduce it.


Instructions
------------

To compile the documentation, you need a python environment with sphinx.

.. code-block:: bash

    $ conda activate myenv
    (myenv)$ cd report
    (myenv)$ make html

The resulting document should be in **report/build/html/index.html**

If you want to replay the analysis, all the scripts that generated the figures
are in the **script** folder.
