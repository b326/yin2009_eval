"""
Plot fig2a
==========

CO2 fixation rate for different levels of irradiance and leaf temperature
"""
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from yin2009 import pth_clean, raw

try:
    rad_ref = int(sys.argv[1])
except IndexError:
    rad_ref = 250

cc = 245  # [µbar]

# table2
table2 = pd.read_csv(pth_clean / "table2.csv", sep=";", comment="#", index_col=['constant'])

tab2 = table2['value'].to_dict()

# read data
meas = pd.read_csv(pth_clean / "fig2.csv", sep=";", comment="#")
meas = meas[meas['cc'] == cc]

# from fig6
oxygen = 210000  # [µbar]
vcmax_25 = 120  # [µmol CO2.m-2.s-1]
jmax_25 = 230  # [µmol.m-2.s-1]
rd_25 = 0.01 * vcmax_25  # [µmol CO2.m-2.s-1]

# scalings
if rad_ref == 250:
    g_sca, k_sca, jmax_25_sca = [1.78642994, 1.51040318, 1.50804385]
elif rad_ref == 500:
    g_sca, k_sca, jmax_25_sca = [1.38681926, 1.09611647, 1.36394918]
elif rad_ref == 1000:
    g_sca, k_sca, jmax_25_sca = [1.24707656, 0.59126494, 1.37517465]
else:
    raise UserWarning(f"unrecognized irradiance '{rad_ref:d}")

# compute carbon assimilation
dfs = []
for i_abs in (250, 500, 1000):  # [µmol photon.m-2.s-1]
    records = []
    for t_leaf in np.linspace(5, 40, 100):  # [°C]
        kappa_ll = raw.eq13(tab2['s'], tab2['phi2_ll'])

        kmc = raw.eq5(t_leaf, tab2['kmc_25'], tab2['kmc_act'])
        kmo = raw.eq5(t_leaf, tab2['kmo_25'], tab2['kmo_act'])
        sco = raw.eq5(t_leaf, tab2['sco_25'], tab2['sco_act'] * g_sca)
        rd = raw.eq5(t_leaf, rd_25, tab2['rd_act'])
        jmax = raw.eq6(t_leaf, jmax_25 * jmax_25_sca, tab2['jmax_act'], tab2['jmax_deact'], tab2['jmax_s'])
        vcmax = raw.eq5(t_leaf, vcmax_25, tab2['vcmax_act'])

        gamma_star = raw.eq7(oxygen, sco)
        j_val = raw.eq14(kappa_ll * k_sca, i_abs, jmax, tab2['theta'])

        # rubisco limited
        x1 = vcmax
        x2 = kmc * (1 + oxygen / kmo)
        an_rub = raw.eq18(cc, gamma_star, x1, x2, rd)

        # electron transport limited
        x1 = j_val / 4
        x2 = 2 * gamma_star
        an_elec = raw.eq18(cc, gamma_star, x1, x2, rd)

        records.append(dict(
            t_leaf=t_leaf,
            an=min(an_rub, an_elec),
            an_rub=an_rub,
            an_elec=an_elec,
        ))

    df = pd.DataFrame(records).set_index('t_leaf')

    dfs.append((i_abs, df))

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 5), squeeze=False)
ax = axes[0, 0]

for i_abs, df in reversed(dfs):
    smeas = meas[meas['i_abs'] == i_abs]
    crv, = ax.plot(smeas['t_leaf'], smeas['an'], label=f"{i_abs:0.0f} (Yin 2009)")
    ax.plot(df.index, df['an'], ls='--', color=crv.get_color(), label="our sim")
    # ax.plot(df.index, df['an_rub'], ls='-.', color=crv.get_color())
    # ax.plot(df.index, df['an_elec'], ls=':', color=crv.get_color())

ax.legend(loc='upper left', title="irradiance [µmol photon.m-2.s-1]")
ax.set_xlabel("t_leaf [°C]")
ax.set_ylabel("Net carbon assimilation [µmol CO2.m-2.s-1]")
fig.suptitle(f"Fitted on: {rad_ref:d} [µmol photon.m-2.s-1]")

fig.tight_layout()
if sys.argv[-1] == "batch":
    fig.savefig(f"../report/fig/fig2a_fitted_{rad_ref:04d}.svg")
else:
    plt.show()
