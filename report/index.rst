====================================================
Welcome to yin2009_eval's documentation!
====================================================

.. toctree::
    :caption: Contents
    :maxdepth: 2

    introduction
    fig2

.. toctree::
    :caption: Annexe
    :maxdepth: 1

    gitlab home <https://gitlab.com/b326/yin2009_eval>
    authors
    badges <badges/listing>
