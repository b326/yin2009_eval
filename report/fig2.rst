Figure 2
========

Parameters
----------

Parameters where taken from table 2, legend of figure 2 and legend of figure 6.

.. list-table:: Parameters values and provenance
  :widths: 25 25 25 25
  :header-rows: 1

  * - Name
    - Value
    - Unit
    - Provenance
  * - cc
    - 245
    - [µbar]
    - figure2
  * - oxygen
    - 210000
    - [µbar]
    - figure6
  * - Vc_max_25
    - 120
    - [µmol CO2.m-2.s-1]
    - figure6
  * - J_max_25
    - 230
    - [µmol e-.m-2.s-1]
    - figure6
  * - rd_25
    - 0.01 Vc_max_25
    - [µmol CO2.m-2.s-1]
    - figure6

All the other values where taken from Table2.

Default simulation
------------------

Using default parameters (using middle of the given interval when necessary) we
obtain the following result for the upper part (fig2 a only).

.. figure:: fig/fig2a_avg.svg

Using the maximum and minimum values found for E_j_max and s in Table 2, we obtain
the following results.

.. figure:: fig/fig2a_min.svg

.. figure:: fig/fig2a_max.svg

Fitting of parameters
---------------------

Since none of the evaluation are close to the 'official' curves, we tried to fit
parameters to try and get close to the one in the article.

The parameters we fitted were: sco_act, kappa_ll and J_max_25.
All three of them were allowed to vary simultaneously in an interval ranging from
half to three times their default value.

.. figure:: fig/fig2a_fitted_0250.svg

.. figure:: fig/fig2a_fitted_0500.svg

.. figure:: fig/fig2a_fitted_1000.svg

Conclusion
----------

None of the set of parameters we tried produce a result close, globally, to the curve
displayed in the article.

In particular:

  - We sometime observe an abrupt change of slope when the system transition from
    Rubisco limited to electron transport limited.
    However, this phenomenon is never observed on the curves in the article.
  - We haven't been able to reproduce the relative space between the different curve,
    i.e. the distance between 500 and 1000 is way smaller than the distance between
    250 and 500.

Fitting the parameters to reproduce one level of irradiance in particular does not
provide a better solution for the other levels of irradiance also.
